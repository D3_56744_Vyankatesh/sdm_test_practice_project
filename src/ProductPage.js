import axios from 'axios'
import { useState, useEffect } from 'react'
import './page1.css'

const ProductPage = () => {
    const [products, setProducts] = useState([])

    useEffect(() => {
        getAllProducts()
    }, [])

    const getAllProducts = () => {
        const url = `http://localhost:4000/product`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setProducts(result['data'])
            }
        })
    }
    return (
        <div id='container'>
            <h1>Product List</h1>

            {
                products.map((product) => {
                    return (<div>
                            <h2>Title:{product['title']}</h2>
                            <p>Price:{product['price']}</p>
                            </div>)
                })
            }

        </div>
    )
}

export default ProductPage